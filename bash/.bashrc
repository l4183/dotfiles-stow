#######################################
# Welcome
#######################################

[ -f /etc/bashrc ] && . /etc/bashrc

if [[ -z "$TMUX" ]] ;then
#    ID="`tmux ls | grep -vm1 attached | cut -d: -f1`" # get the id of a deattached session
    ID="`tmux ls | cut -d: -f1`" # get the id of a deattached session
    if [[ -z "$ID" ]] ;then # if not available create a new one
        tmux new-session
    else
        tmux attach-session -d -t "$ID" # if available attach to it
    fi
fi

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

if [ -f ~/.bashrc.aliases ]; then
	. ~/.bashrc.aliases
fi

export LC_ALL=en_US.UTF-8;
export LANG=en_US.UTF-8
unset LANGUAGE;

export XDG_CONFIG_HOME="$HOME/.config"
export PATH=$PATH:/sbin:/usr/sbin:/usr/local/sbin:/home/$USER/bin:home/$USER/.local/bin:/home/$USER/scripts

# No noise
set bell-style visible
set -o vi

# Let's have core dumps
ulimit -c unlimited

#######################################
# user specific environment
#######################################

have()
{
    unset -v have
    PATH=$PATH:/sbin:/usr/sbin:/usr/local/sbin:/home/$USER/bin:home/$USER/.local/bin type $1 >&/dev/null && have="yes"
}

for f in `ls /usr/share/bash-completion/completions`; do source /usr/share/bash-completion/completions/$f; done

#######################################
# history
#######################################

# don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
HISTCONTROL=$HISTCONTROL${HISTCONTROL+:}ignoredups
HISTCONTROL=ignoreboth
HISTIGNORE='ls:bg:fg:history'
HISTTIMEFORMAT='%F %T '
PROMPT_COMMAND='history -a; history -n'

# append to the history file, don't overwrite it
shopt -s histappend

#Bigger history file size
unset HISTFILESIZE
HISTSIZE=1000

#######################################
# functions
#######################################

## FUNCTIONS
# make and change to a directory
md () { mkdir -p "$1" && cd "$1"; }

# print the corresponding error message
strerror() { python -c "\
import os,locale as l; l.setlocale(l.LC_ALL, ''); print os.strerror($1)"; }

# quick plot of numbers on stdin. Can also pass plot params.
# E.G: seq 1000 | sed 's/.*/s(&)/' | bc -l | plot linecolor 2
plot() { { echo 'plot "-"' "$@"; cat; } | gnuplot -persist; }

# highlight occurences of expression
hili() { e="$1"; shift; grep --col=always -Eih "$e|$" "$@"; }

# Ditto, but user pager ('n' to iterate)
scan() { e="$1"; shift; less -i -p "*$e" "$@"; }

# Make these functions, common to other languages, available in the shell
ord() { printf "0x%x\n" "'$1"; }
chr() { printf $(printf '\\%03o\\n' "$1"); }

#######################################
# welcome
#######################################

INPUT_COLOR="\[\033[0m\]"
DIR_COLOR="\[\033[0;33m\]"
DIR="\w"

LINE_VERTICAL="\342\224\200"
LINE_CORNER_1="\342\224\214"
LINE_CORNER_2="\342\224\224"
LINE_COLOR="\[\033[0;37m\]"

USER_NAME="\[\033[0;32m\]\u"
SYMBOL="\[\033[0;32m\]>"

if [[ ${EUID} == 0 ]]; then
   USER_NAME="\[\033[0;31m\]\u"
   SYMBOL="\[\033[0;31m\]#"
fi

PS1="$LINE_COLOR$LINE_CORNER_1$LINE_VERTICAL \h$ $DIR_COLOR$DIR \n$LINE_COLOR$LINE_CORNER_2$LINE_VERTICAL $SYMBOL $INPUT_COLOR"
PS1=" $DIR_COLOR$DIR \n $SYMBOL $INPUT_COLOR"

export PS1="\[\e[1m\][ \u ] [ \h ] [ \w ]\n\\$\[\e[0m\] \[$(tput sgr0)\]"
export PS1="\[\e[1m\][ \u ] [ \w ]\n\\$\[\e[0m\] \[$(tput sgr0)\] "
