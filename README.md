## Dotfiles Stow

---

Uses GNU Stow – https://www.gnu.org/software/stow/

### Usage

```
cd ~
```

```
git clone https://gitlab.com/l4183/dotfiles-stow
```

```
cd dotfiles-stow
```

```
stow bash (or tmux, etc.)
```

_or for all configs_

```
stow -v */
```

--------------------

moslty designed to be used with https://gitlab.com/l4183/linux-infr
